from django.contrib import admin

from .models import Shoe, BinsVO

@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    pass

@admin.register(BinsVO)
class BinsVOAdmin(admin.ModelAdmin):
    pass

from django.urls import path

from .views import api_list_hats, api_list_location_vos, api_delete_hat

urlpatterns = [
    path("hats/", api_list_hats, name="api_list_hats"),
    path("locations/", api_list_location_vos, name="api_list_location_vos"),
    path("hats/<int:pk>", api_delete_hat, name="api_delete_hat"),
]

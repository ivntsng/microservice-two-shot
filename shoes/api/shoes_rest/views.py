from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Shoe, BinsVO
import json


class BinsVODetailedEncoder(ModelEncoder):
    model = BinsVO
    properties = [
        "import_href",
        "closet_name",
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer_name",
        "model_name",
        "color",
        "picture_url",
    ]
    def get_extra_data(self, o):
        return {'bin': o.bin.closet_name}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        'manufacturer_name',
        'model_name',
        'color',
        'picture_url',
        'bin',
        "id"
    ]
    encoders = {
        'bin': BinsVODetailedEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            desired_bin = content["bin"]
            bin = BinsVO.objects.get(import_href=desired_bin)
            content["bin"] = bin
        except BinsVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_shoe_detail(request, pk):
    if request.method == "GET":
        shoe = get_object_or_404(Shoe, id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    elif request.method == "PUT":
        content = json.loads(request.body)
        shoe = get_object_or_404(Shoe, id=pk)

        props = ["manufacturer_name", "model_name", "color", "picture_url", "bin", "id"]
        for prop in props:
            if prop in content:
                setattr(shoe, prop, content[prop])
        shoe.save()
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        return JsonResponse({"message": "Method not allowed"}, status=405)

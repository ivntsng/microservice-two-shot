import { NavLink, Link } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-info">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          Wardrobify
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">
                Home
              </NavLink>
            </li>
            <div className="dropdown">
              <button
                className="btn btn-transparent dropdown-toggle"
                type="button"
                id="shoeMenuButton"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Shoes
              </button>
              <ul className="dropdown-menu" aria-labelledby="shoeMenuButton">
                <li>
                  <Link className="dropdown-item" to="/shoes">
                    List of Shoes
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/shoes/new">
                    New shoe
                  </Link>
                </li>
              </ul>
              <button className="btn bg-transparent dropdown-toggle" type="button" id="hatMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                Hats
              </button>
              <ul className="dropdown-menu" aria-labelledby="hatMenuButton">
                <li><Link className="dropdown-item" to="/hats">List of Hats</Link></li>
                <li><Link className="dropdown-item" to="/hats/new">New Hat</Link></li>
              </ul>
            </div>
          </ul>
        </div>

      </div>
    </nav>
  );
}

export default Nav;

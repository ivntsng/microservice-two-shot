import { Link } from 'react-router-dom';

function HatList({ hats, getHats }) {

    async function handleDelete(event) {
        event.preventDefault();
        const hatUrl = `http://localhost:8090/api/hats/${event.target.value}`;

        const fetchConfig = {
            method: "delete",
        }

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            getHats();
        }
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Color</th>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Location</th>
                    <th>Delete?</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={`${hat.id}+${hat.style}`}>
                            <td>{hat.color}</td>
                            <td>{hat.fabric}</td>
                            <td>{hat.style}</td>
                            <td>{hat.location}</td>
                            <td>
                                <button onClick={handleDelete} value={hat.id}>
                                    Delete
                                </button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default HatList;

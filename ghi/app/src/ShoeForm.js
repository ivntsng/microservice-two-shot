import React, { useEffect, useState } from "react";

export default function ShoeForm({ getShoes }) {
  const [manufacturerName, setManufacturerName] = useState("");
  const [modelName, setModelName] = useState("");
  const [color, setColor] = useState("");
  const [picture_url, setPicture] = useState("");
  const [bin, setBin] = useState("");
  const [bins, setBins] = useState([]);

  const handleManufacturerChange = (e) => {
    const value = e.target.value;
    setManufacturerName(value);
  };
  const handleModelNameChange = (e) => {
    const value = e.target.value;
    setModelName(value);
  };
  const handleColorChange = (e) => {
    const value = e.target.value;
    setColor(value);
  };
  const handlePictureChange = (e) => {
    const value = e.target.value;
    setPicture(value);
  };
  const handleBinChange = (e) => {
    const value = e.target.value;
    setBin(value);
  };

  useEffect(() => {
    async function getBins() {
      const url = "http://localhost:8100/api/bins";
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setBins(data.bins);
      }
    }
    getBins();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {};
    data.manufacturer_name = manufacturerName;
    data.model_name = modelName;
    data.color = color;
    data.picture_url = picture_url;
    data.bin = bin;

    const url = `http://localhost:8080/api/shoes/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "applications/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      setManufacturerName("");
      setModelName("");
      setColor("");
      setPicture("");
      getShoes();
      setBin("");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create new shoes</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleManufacturerChange}
                value={manufacturerName}
                placeholder="manufacturer_name"
                required
                type="text"
                name="manufacturer_name"
                id="manufacturer_name"
                className="form-control"
              />
              <label htmlFor="manufacturer_name">Manufacturer name</label>
            </div>
            <div className="form-floating mb-3">
              <textarea
                onChange={handleModelNameChange}
                value={modelName}
                placeholder="model"
                required
                row="3"
                id="model"
                className="form-control"
                name="model"
              ></textarea>
              <label htmlFor="model">Model name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleColorChange}
                value={color}
                placeholder="color"
                required
                type="text"
                id="color"
                className="form-control"
                name="color"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handlePictureChange}
                value={picture_url}
                placeholder="picture_url"
                required
                type="URL"
                id="picture"
                className="form-control"
                name="picture_url"
              />
              <label htmlFor="picture_url">Picture url</label>
            </div>
            <div className="mb-3">
              <select
                onChange={handleBinChange}
                value={bin}
                required
                id="bin"
                type="text"
                name="bin"
                className="form-select"
              >
                <option value="">Choose Bin</option>
                {bins.map((bin) => {
                  return (
                    <option key={bin.id} value={bin.href}>
                      {bin.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary" type="submit">
              Create
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

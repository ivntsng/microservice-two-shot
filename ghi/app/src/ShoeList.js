import "./index.css";
export default function ShoesList({ shoeList, getShoes }) {
  const deleteShoe = (id) => {
    fetch(`http://localhost:8080/api/shoes/${id}/`, {
      method: "delete",
    })
      .then(() => {
        return getShoes();
      })
      .catch(console.log);
  };

  if (shoeList === undefined) {
    return null;
  }

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Manufacturer Name</th>
          <th>Model Name</th>
          <th>Color</th>
          <th>Picture URL</th>
          <th>Bin</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {shoeList.map((shoe) => {
          return (
            <tr key={shoe.id}>
              <td>{shoe.manufacturer_name}</td>
              <td>{shoe.model_name}</td>
              <td>{shoe.color}</td>
              <td>
                <img src={shoe.picture_url} width="150" height="150"></img>
              </td>
              <td>{shoe.bin}</td>
              <td>
                <button onClick={() => deleteShoe(shoe.id)}>Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

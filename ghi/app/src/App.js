import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import HatForm from "./HatForm";
import HatList from "./HatList";
import ShoeDelete from "./ShoeDelete";
import ShoeForm from "./ShoeForm";
import ShoeList from "./ShoeList";
import { useEffect, useState } from "react";

function App() {
  const [hats, setHats] = useState([]);
  const [shoes, setShoes] = useState([]);

  async function getHats() {
    const hatUrl = "http://localhost:8090/api/hats/";
    const response = await fetch(hatUrl);
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  }

  async function getShoes() {
    const shoeUrl = "http://localhost:8080/api/shoes/";
    const response = await fetch(shoeUrl);
    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
    }
  }

  useEffect(() => {
    getHats();
    getShoes();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route
              index
              element={<ShoeList shoeList={shoes} getShoes={getShoes} />}
            />
            <Route path="new" element={<ShoeForm getShoes={getShoes} />} />
            <Route path="delete" element={<ShoeDelete shoes={shoes} />} />
          </Route>
          <Route path="hats">
            <Route index element={<HatList hats={hats} getHats={getHats} />} />
            <Route path="new" element={<HatForm getHats={getHats}/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

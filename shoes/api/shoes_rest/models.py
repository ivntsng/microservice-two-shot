from django.db import models

from django.urls import reverse

class BinsVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.closet_name

class Shoe(models.Model):
    manufacturer_name = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(max_length=255, null=True)

    bin = models.ForeignKey(
        BinsVO,
        related_name="shoes",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.model_name

# Wardrobify

Team:

- Ivan Tsang - Shoes
- Tanner Jackson - Hats

## Design

Implemented dropdown menus for shoes and hats using Bootstrap.

## Shoes microservice

BinsVO - Value object based on the "Bin" model in Wardrobe, fetched through polling and contains a "name" field that combines the closet name and bin number with the bin size.



## Hats microservice

LocationVO - a value object based off the Location model in the Wardrobe microservice.
    Pulled in using polling, it includes a "name" that consists of the location's name,
    section number, and shelf number, as well as an "import_href" that refers back to
    the href of the Location model it's derived from.

Hat - an entity that has the properties "style," "fabric," "color," "picture," and
    "location," which must be passed in to create a new hat, as well as a database id.
LocationsVO - a value object based off the Location model in the Wardrobe microservice.
Pulled in using polling, it includes a "name" that consists of the location's name,
section number, and shelf number, as well as an "import_href" that refers back to
the href of the Location model it's derived from.

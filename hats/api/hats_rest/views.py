from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.shortcuts import render
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO

# Create your views here.

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["name", "import_href"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style",
        "fabric",
        "color",
        "id",
        ]

    def get_extra_data(self, o):
        return {"location": str(o.location.name)}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style",
        "fabric",
        "color",
        "picture",
        "id",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"Message": "Invalid location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET"])
def api_list_location_vos(request):
    locations = LocationVO.objects.all()
    return JsonResponse(
        {"locations": locations},
        encoder=LocationVODetailEncoder,
        safe=False,
    )

@require_http_methods(["DELETE"])
def api_delete_hat(request, pk):
    count, _ = Hat.objects.filter(id=pk).delete()
    return JsonResponse({"deleted": count > 0})

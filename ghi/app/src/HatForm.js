import React, { useState, useEffect } from 'react';

function HatForm({ getHats }) {
    const [style, setStyle] = useState('');
    const [fabric, setFabric] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState('');

    useEffect(() => {
        async function getLocations() {
            const url="http://localhost:8090/api/locations/";
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setLocations(data.locations);
            }
        }
        getLocations();
    }, []);

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            style,
            color,
            fabric,
            picture,
            location,
        };

        const hatURL = 'http://localhost:8090/api/hats/'
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const hatResponse = await fetch(hatURL, fetchOptions);
        if (hatResponse.ok) {
            setStyle('');
            setColor('');
            setFabric('');
            setPicture('');
            setLocation('');

            getHats();
        }
    }

    function handleFabricChange(event) {
        const value=event.target.value;
        setFabric(value);
    }

    function handleColorChange(event) {
        const value=event.target.value;
        setColor(value);
    }

    function handleStyleChange(event) {
        const value=event.target.value;
        setStyle(value);
    }

    function handlePictureChange(event) {
        const value=event.target.value;
        setPicture(value);
    }

    function handleLocationChange(event) {
        const value=event.target.value;
        setLocation(value);
    }

    return (
        <div className='row'>
            <div className='offset-3 col-6'>
                <h1>Add a Hat</h1>
                <form onSubmit={handleSubmit} id='create-hat-form'>
                    <div className='form-floating mb-3'>
                        <input onChange={handleStyleChange} value={style} required placeholder='Style' type='text' name='style' id='style' className='form-control' />
                        <label htmlFor='style'>Style</label>
                    </div>
                    <div className='form-floating mb-3'>
                        <input onChange={handleFabricChange} value={fabric} required placeholder='Fabric' type='text' name='fabric' id='fabric' className='form-control' />
                        <label htmlFor='fabric'>Fabric</label>
                    </div>
                    <div className='form-floating mb-3'>
                        <input onChange={handleColorChange} value={color} required placeholder='Color' type='text' name='color' id='color' className='form-control' />
                        <label htmlFor='color'>Color</label>
                    </div>
                    <div className='form-floating mb-3'>
                        <input onChange={handlePictureChange} value={picture} required placeholder='Picture' type='url' name='picture' id='picture' className='form-control' />
                        <label htmlFor='picture'>Picture</label>
                    </div>
                    <div className='mb-3'>
                        <select onChange={handleLocationChange} value={location} required name="location" id="location" className='form-select'>
                            <option value="">Choose a location</option>
                            {locations.map(location => {
                                return (
                                    <option key={location.import_href} value={location.import_href}>{location.name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <button className='btn btn-primary'>Add</button>
                </form>
            </div>
        </div>

    );
}

export default HatForm;
